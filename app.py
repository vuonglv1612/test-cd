from fastapi import FastAPI
import os


app = FastAPI()


@app.get("/")
def hello():
    message = os.getenv("MESSAGE", "Hello, World")
    return {"version": "4", "message": message, "node": os.getenv("HOSTNAME", "")}
